#G2I Trivia project

to run the project with docker first build the image use `npm run docker:build` and after it finished use `npm run docker:run` to run it.
it will be available on `localhost:3000` after `docker:run` finished
if the begin button didn't work , maybe you have allow-access-origin issue.
for resolving that issue i did the ngrok proxy inside our server.
so just route to `/ngrok` and it will automatically redirect you to ngrok instance.