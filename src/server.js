/* eslint-disable no-console */

const Express = require('express');
const compression = require('compression');
const path = require('path');
const cookieParser = require('cookie-parser');
const favicon = require('serve-favicon');
const ngrok = require('ngrok');

const app = new Express();

app.use(compression());
app.use(cookieParser());
app.use(Express.static(path.join(__dirname, '..', 'public')));
app.use(favicon(path.join(__dirname, '..', 'static', 'favicon.ico')));

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '..', 'public', 'index.html'));
});

app.get('/ngrok', async (req, res) => {
  const url = await ngrok.connect(3000);
  res.redirect(url);
});


app.listen(3000, (err) => {
  if (err) {
    console.log(err);
  }
  console.log('server is running on port', 3000);
});

