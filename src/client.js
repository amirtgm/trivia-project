import React from 'react';
import ReactDOM from 'react-dom';
import { browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import { syncHistoryWithStore } from 'react-router-redux';
import ApiClient from './helpers/apiClient';
import createStore from './redux/createStore';
import getRoutes from './routes';

const client = new ApiClient();
const content = document.getElementById('content');
const store = createStore(browserHistory, client);
const history = syncHistoryWithStore(browserHistory, store);
const component = getRoutes(store, history);
ReactDOM.render(
  <Provider store={store} key="provider">
    {component}
  </Provider>, content);
