import React from 'react';
import { Route, Router, IndexRoute } from 'react-router';
import {
  Questions,
  App,
  Result,
  Home,
} from './containers';

function requireBegin(store) {
  return (nextState, replace) => {
    const { questions } = store.getState();
    if (questions && !questions.loaded) {
      replace({ pathname: '/' });
    }
  };
}
function requirePass(store) {
  return (nextState, replace) => {
    const { questions } = store.getState();
    if (questions && !questions.passed) {
      replace({ pathname: '/' });
    }
  };
}
export default(store, history) => (
  <Router history={history}>
    <Route path="/" component={App}>
      <IndexRoute component={Home} />
      <Route path="questions" component={Questions} onEnter={requireBegin(store)} />
      <Route path="result" component={Result} onEnter={requirePass(store)} />
    </Route>
  </Router>
);

