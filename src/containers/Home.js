import React, { PureComponent } from 'react';
import { Row, Col, Button } from 'antd';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { loadQuestions } from '../redux/actions/questions.action';


@connect(
  state => ({
    questions: state.questions,
  }),
  {
    loadQuestions,
  })

export default class Questions extends PureComponent {
  static propTypes = {
    loadQuestions: PropTypes.func,
    questions: PropTypes.shape(),
  };

  componentDidMount() {
    if (!Array.isArray(this.props.questions)) {
      this.getQuestions();
    }
  }

  getQuestions() {
    this.props.loadQuestions();
  }

  render() {
    return (
      <Row type="flex" justify="center" align="top">
        <Col span={12} className="home-container">
          <h2 className="title">Welcome to the Trivia Challange!</h2>
          <p className="text">You will be presented with 10 True or False questions.</p>
          <p className="text">Can you score 100%?</p>
          <Link to="questions">
            <Button
              type="primary"
              size={'large'}
              className="button"
              onClick={this.begin}
            >
              BEGIN
            </Button>
          </Link>
        </Col>
      </Row>
    );
  }
}
