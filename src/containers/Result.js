import React, { PureComponent } from 'react';
import { Row, Col, Button, Icon } from 'antd';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { loadQuestions } from '../redux/actions/questions.action';
// import { ImageComponent, GalleryFilter } from '../components';


@connect(
  state => ({
    questions: state.questions,
  }),
  {
    loadQuestions,
  })

export default class Questions extends PureComponent {
  static propTypes = {
    questions: PropTypes.shape(),
  };
  state = {
    totalCorrect: 0,
  }

  componentWillMount() {
    const { questions } = this.props;
    const totalCorrect = questions.results.filter(question =>
      question.user_answer === question.correct_answer).length;
    this.setState({
      totalCorrect,
    });
  }

  render() {
    const { questions } = this.props;

    return (
      <Row type="flex" justify="center" align="top">
        <Col span={18} className="result-title">
          <h1>Result</h1>
          <h3>Score : {this.state.totalCorrect} from 10</h3>
        </Col>
        <Col span={18} className="result-container">
          <Row type="flex" justify="left" align="middle" className="head-line">
            <Col span={2}>
            Is Correct?
            </Col>
            <Col span={17}>
            Question
            </Col>
            <Col span={5}>
              Your answer
            </Col>
          </Row>
          { questions && questions.results.map((question) => {
            const isCorrect = question.correct_answer === question.user_answer;
            return (
              <Row key={`result-item-${question.id}`} type="flex" justify="left" align="middle" className="item-row">
                <Col span={2}>
                  {isCorrect && <Icon type="check" />}
                  {!isCorrect && <Icon type="close" />}
                </Col>
                <Col span={17} className="question" dangerouslySetInnerHTML={{ __html: question.question }} />
                <Col span={5} className="button-wrapper">
                  <Button type="primary" size={'large'} className={`button ${isCorrect && 'active'}`}>True</Button>
                  <Button type="primary" size={'large'} className={`button ${!isCorrect && 'active'}`}>False</Button>
                </Col>
              </Row>
            );
          })}
        </Col>
      </Row>
    );
  }
}
