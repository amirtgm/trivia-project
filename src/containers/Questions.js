import React, { PureComponent } from 'react';
import { Row, Col, Button } from 'antd';
import { browserHistory } from 'react-router';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { answerAction, passed } from '../redux/actions/questions.action';

@connect(
  state => ({
    questions: state.questions,
    router: state.routing,
  }),
  {
    answerAction, passed,
  })

export default class Questions extends PureComponent {
  static propTypes = {
    answerAction: PropTypes.func,
    passed: PropTypes.func,
    questions: PropTypes.shape(),
  };
  constructor(props) {
    super(props);
    this.answerHandler = this.answerHandler.bind(this);
  }
  state = {
    questionNumber: 0,
  }
  answerHandler(result) {
    const { questions } = this.props;
    this.props.answerAction({
      result,
      id: questions.results[this.state.questionNumber].id,
    });
    if (this.state.questionNumber < (questions.results.length - 1)) {
      this.setState({ questionNumber: this.state.questionNumber + 1 });
    } else {
      this.props.passed();
      browserHistory.push('/result');
    }
  }
  render() {
    const { questions } = this.props;
    const data = questions.results[this.state.questionNumber];
    return (
      <Row type="flex" justify="center" align="top">
        <Col span={12} className="questions-container">
          <span className="category">{data.category}</span>
          <h2
            className="title"
            dangerouslySetInnerHTML={{ __html: data.question }}
          />
          <Row type="flex" justify="end" align="middle">
            <Col span={24} className="button-wrapper">
              <Button type="primary" size={'large'} className="button" onClick={() => this.answerHandler(true)}>True</Button>
              <Button type="primary" size={'large'} className="button" onClick={() => this.answerHandler(false)}>False</Button>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }
}
