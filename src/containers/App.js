import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export default class ImageComponent extends PureComponent {
    static propTypes = {
      children: PropTypes.element,
    };

    render() {
      return (
        <div>
          {this.props.children}
        </div>
      );
    }
}
