export function loadQuestions() {
  return {
    types: 'load_questions',
    promise: client => client.get('api.php?amount=10&difficulty=hard&type=boolean'),
  };
}
export function passed() {
  return {
    type: 'PASSED',
  };
}
export function answerAction(payload) {
  return {
    type: 'ANSWER',
    payload,
  };
}
