import ActionTypes from '../actionTypes';

const initialState = {
  loaded: false,
  passed: false,
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case ActionTypes.LOAD_QUESTIONS:
      return {
        ...state,
        ...action.load,
      };
    case ActionTypes.LOAD_QUESTIONS_SUCCESS:
      return {
        ...state,
        ...action.load,
        results: action.result.results.map((data, id) => ({
          ...data,
          id,
          user_answer: null,
          correct_answer: data.correct_answer === 'True',
        })),
      };
    case ActionTypes.LOAD_QUESTIONS_FAIL:
      return {
        ...state,
        ...action.load,
      };
    case ActionTypes.PASSED:
      return {
        ...state,
        passed: true,
      };
    case ActionTypes.ANSWER:
      return {
        ...state,
        results: state.results.map(data => ({
          ...data,
          user_answer: action.payload.id === data.id ? action.payload.result : data.user_answer,
        })),
      };
    default:
      return state;
  }
}
