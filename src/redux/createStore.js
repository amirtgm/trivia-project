import { createStore as _createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import createMiddleware from '../helpers/clientMiddleware';
import reducer from './reducers';

export default function createStore(history, client, data) {
  const reduxRouterMiddleware = routerMiddleware(history);
  const middleware = [createMiddleware(client), reduxRouterMiddleware];
  const finalCreateStore = compose(applyMiddleware(...middleware))(_createStore);
  const store = finalCreateStore(reducer, data);
  return store;
}
