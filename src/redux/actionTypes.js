import createconstants from '../utils/createConstants';
import create from '../utils/actionTypesFormat';

export default createconstants([
  create('questions'),
  create('answer'),
  create('passed'),
]);
