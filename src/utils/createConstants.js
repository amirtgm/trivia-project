import flatten from 'lodash.flatten';

export default function createConstants(constants) {
  return flatten(constants).reduce((acc, constant) => {
    acc[constant] = constant;
    return acc;
  }, {});
}
