export default function (string = '') {
  const str = string.toUpperCase();

  return [
    `${str}`,
    `${str}_SUCCESS`,
    `${str}_FAIL`,
    `CREATE_${str}`,
    `CREATE_${str}_SUCCESS`,
    `CREATE_${str}_FAIL`,
    `LOAD_${str}`,
    `LOAD_${str}_SUCCESS`,
    `LOAD_${str}_FAIL`,
    `EDIT_${str}`,
    `EDIT_${str}_SUCCESS`,
    `EDIT_${str}_FAIL`,
    `DELETE_${str}`,
    `DELETE_${str}_SUCCESS`,
    `DELETE_${str}_FAIL`,
  ];
}
